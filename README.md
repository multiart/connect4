# Connect Four

Connect Four board game implemenation with Django, Channels and Vue.js



$ git clone https://gitlab.com/multiart/connect4.git  
$ cd connect4  
$ pipenv install  
$ parcel build vue/index.js --out-dir game/static --out-file main.js  
$ pipenv run python manage.py runserver  

Open two browser's windows or tabs and enter URL:  
[http://127.0.0.1:8000](http://127.0.0.1:8000)
