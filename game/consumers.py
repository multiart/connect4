from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer

from .connect4 import Connect4

connect4 = Connect4()


class GameConsumer(JsonWebsocketConsumer):

    def connect(self):
        connect4.player_join(self)

    def disconnect(self, close_code):
        connect4.player_disconnect(self)

    def receive_json(self, data):
        if 'click' in data:
            column = data.get('click')
            connect4.player_click(self, column)
