class Connect4:
    counter = 0
    board = None
    players = {'red': None, 'yellow': None}
    player = 'red'

    def __init__(self):
        self.reset()

    def reset(self):
        self.players['red'] = None
        self.players['yellow'] = None
        self.player = 'red'
        self.board = [['white' for i in range(6)] for j in range(7)]

    def is_both_players_connected(self):
        return self.players['red'] is not None and self.players['yellow'] is not None

    def player_join(self, consumer):
        if self.players['red'] is None:
            self.players['red'] = consumer
            consumer.accept()
            consumer.send_json({'color': 'red'})
        elif self.players['yellow'] is None:
            self.players['yellow'] = consumer
            consumer.accept()
            consumer.send_json({'color': 'yellow'})

        if self.is_both_players_connected():
            self.send_to_players('turn', self.player)
            self.send_to_players('board', self.board)

    def player_disconnect(self, consumer):
        if self.players['red'] == consumer:
            self.players['red'] = None
            print('player red disconnect')

        if self.players['yellow'] == consumer:
            self.players['yellow'] = None

    def player_click(self, consumer, column):
        if self.players[self.player] != consumer:
            print('click from wrong player: ' +
                  'yellow' if self.player == 'red' else 'red')
            return

        if not self.is_both_players_connected():
            print('click before all players conected')
            return

        row = -1
        for row in range(5, -1, -1):
            if self.board[column][row] == 'white':
                self.board[column][row] = self.player
                break

        self.send_to_players('board', self.board)

        if self.check_victory(column, row):
            self.send_to_players('victory', self.player)
            self.reset()
            return

        self.player = 'red' if self.player == 'yellow' else 'yellow'
        self.send_to_players('turn', self.player)

    def send_to_players(self, command, message):
        self.players['red'].send_json({command: message})
        self.players['yellow'].send_json({command: message})

    def check_victory(self, col, row):

        def get_cell(col, row):
            if col < 0 or row < 0:
                return None
            if col > 7 or row > 5:
                return None
            return self.board[col][row]

        cell = get_cell(col, row)

        cnt = 0
        for i in range(1, 4):
            if get_cell(col - i, row) != cell:
                break
            cnt += 1
        for i in range(1, 4):
            if get_cell(col + i, row) != cell:
                break
            cnt += 1
        if cnt > 2:
            return True

        cnt = 0
        for i in range(1, 4):
            if get_cell(col, row - i) != cell:
                break
            cnt += 1
        for i in range(1, 4):
            if get_cell(col, row + i) != cell:
                break
            cnt += 1
        if cnt > 2:
            return True

        cnt = 0
        for i in range(1, 4):
            if get_cell(col - i, row - i) != cell:
                break
            cnt += 1
        for i in range(1, 4):
            if get_cell(col + i, row + i) != cell:
                break
            cnt += 1
        if cnt > 2:
            return True

        cnt = 0
        for i in range(1, 4):
            if get_cell(col - i, row + i) != cell:
                break
            cnt += 1
        for i in range(1, 4):
            if get_cell(col + i, row - i) != cell:
                break
            cnt += 1
        if cnt > 2:
            return True

        return False
